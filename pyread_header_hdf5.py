#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""Routine for reading Gadget3 HDF5 Headers."""

import numpy as np
import h5py
from collections import OrderedDict

from numpy import uint8, uint32, uint64, float32, float64

__author__ = 'Alan Duffy'
__email__ = 'mail@alanrduffy.com'
__version__ = '0.1.0'


def pyread_header_hdf5(filename, silent=None, fofheader=None, subheader=None, bugfix=None):
    """ Read Header information of Gadget3 outputs (Snapshots, FoF and SubFind) """

    """
    +
     NAME:
           PYREAD_HEADER_HDF5
    
     PURPOSE:
           This function reads in HDF5 header outputs (default snapshot, user select FOF and SubFind) 
           from the OWLS Gadget3 code. Note Snapshot header can read in for SubFind files too.
    
     CATEGORY:
           I/O, HDF5, OWLS / PAISTE :)
    
     REQUIREMENTS:
            import h5py
            import numpy
            from numpy import uint8, uint32, uint64, float32, float64
            from collections import OrderedDict

     CALLING SEQUENCE:
        from pyread_header_hdf5 import *
        Result = pyread_header_hdf5(filename [, silent=True, fofheader=True, subheader=True, bugfix=True] )
    
     INPUTS:
           filename: Name of the file to read in. The routine will read the header 
                     of only the file given (i.e. no looping over sub-files). 

     OPTIONAL INPUTS:
    
     KEYWORD PARAMETERS (set to True)
           silent:       does not print any message on the screen
           bugfix:       If selected then NumPart_Total set to NumPart_ThisFile (bug in SubFind output that
                         doesn't initialise the output of NumPart_Total when only one subfile is selected)
           fofheader:    Read in the FOF Header info
           subheader:    Read in the SubFind Header info
           [if neither fofheader or subheader set to True then snapshot info is assumed]
    
     OUTPUTS:
            An OrderedDict of Header attribute names and their values from the Gadget3 Output. 
    
     RESTRICTIONS:
            Flag_Entropy_ICs not read in... assumed empty and set to zero for Gadget3
            
     PROCEDURE:
    
    
     EXAMPLE:
        
            Reading Number of DM particles in snapshot header from SubFind output:
    
            Header = pyread_header_hdf5('subhalo_014.0.hdf5', silent=silent)
            print Header['NumPart_ThisFile'][1].astype(uint32)

        
            Reading Number of FOF Groups in the SubFind output:
            Header = pyread_header_hdf5('subhalo_014.0.hdf5', silent=silent, fofheader=True)
            print Header['Number_of_groups'].astype(uint32)

    
            Reading Number of SubFind subhaloes in the SubFind output:
            Header = pyread_header_hdf5('subhalo_014.0.hdf5', silent=silent, subheader=True)
            print Header['Number_of_subgroups'].astype(uint32)

        
            Reading Number of DM particles in snapshot header from Snapshot output:
    
            Header = pyread_header_hdf5('snapshot_014.0.hdf5', silent=silent)
            print Header['NumPart_ThisFile'][1].astype(uint32)

     MODIFICATION HISTORY (by Alan Duffy):
            
            16/09/13 Created pyread_header_hdf5 routine
            16/09/13 Added bugfix keyword, this is by default turned off as it sets NumPart_Total = NumPart_ThisFile
            to resolve an issue with the NumPart_Total array not being initialised when only one file outputted.
            Any issues please contact Alan Duffy on mail@alanrduffy.com or (preferred) twitter @astroduff
    """

#########################################################
##
## Check user choices...
##
#########################################################

    if fofheader != None:
        if silent == None:
            print "User reading in FoF Group Header Information "
        
        filedir = 'FOF'
        
        BINARY_GADGET_HEADER_NAMES= ( \
            'NTask', 'Number_of_groups',  'Number_of_subgroups', 'Number_per_Type', 
            'Total_Number_of_groups',  'Total_Number_of_subgroups', 'Total_Number_per_Type')
        
        BINARY_GADGET_HEADER_SIZES= (\
            (uint32, 1), (uint32, 1), (uint32, 1), (uint32,6), 
            (uint32, 1), (uint32, 1), (uint32,6))
    elif subheader != None:
        if silent == None:
            print "User reading in SubFind Group Header Information "
        
        filedir = 'SUBFIND'
        
        BINARY_GADGET_HEADER_NAMES= ( \
            'Number_of_groups',  'Number_of_subgroups', 'Number_per_Type', 
            'Total_Number_of_groups',  'Total_Number_of_subgroups', 'Total_Number_per_Type')
        
        BINARY_GADGET_HEADER_SIZES= (\
            (uint32, 1), (uint32, 1), (uint32,6), 
            (uint32, 1), (uint32, 1), (uint32,6))
    else:
        if silent == None:
            print "User reading in Gadget Header Information "
        
        filedir='Header'
        
        BINARY_GADGET_HEADER_NAMES= ( \
            'NumPart_ThisFile', 'MassTable', 'ExpansionFactor', 'Redshift', 
            'Flag_Sfr', 'Flag_Feedback', 'NumPart_Total', 'Flag_Cooling', 
            'NumFilesPerSnapshot', 'BoxSize', 'Omega0', 'OmegaBaryon', 'OmegaLambda', 
            'HubbleParam', 'Flag_StellarAge', 'Flag_Metals', 'NumPart_Total_HighWord', 
            'Flag_Entropy_ICs', 'Filler')

        BINARY_GADGET_HEADER_SIZES= (\
            (uint32,6), (float64, 6), (float64, 1), (float64, 1), 
            (uint32, 1), (uint32,1), (uint32, 6), (uint32,1), 
            (uint32,1), (float64, 1), (float64, 1), (float64, 1), (float64, 1), 
            (float64, 1), (uint32,1), (uint32, 1), (uint32, 6), 
            (uint32, 1), (uint8, 60))        
    if silent == None:
        print "Reading Header from "+filename

    with h5py.File(filename, "r") as fin:
        Header= OrderedDict()
        for key, size in zip(BINARY_GADGET_HEADER_NAMES,BINARY_GADGET_HEADER_SIZES):
            if key == 'Flag_Entropy_ICs' or key == 'Filler':
                Header[key]= np.zeros(size[1], dtype=size[0])
            elif key == 'NumPart_Total' and sum(fin[filedir].attrs[key].astype(size[0])) < 1. and bugfix != None:
            ## A Bug in some one file outputs that NumPart_Total not initialised
                Header[key]= fin[filedir].attrs['NumPart_ThisFile'].astype(size[0])                    
            else:
                Header[key]= fin[filedir].attrs[key].astype(size[0])

    return Header